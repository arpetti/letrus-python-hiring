# Letrus Teste de Habilidades Práticas (Back-end)
Esse é um teste de conhecimento em Python do time de desenvolvimento da Letrus.

Nesse teste, queremos que você implemente uma forma de gerar HTML com base em
uma lista de erros e textos armazenados em um banco de dados.

Fornecemos um banco de dados SQLite populado com um texto e sua lista de erros
ortográficos.

Sua tarefa é:
- Implementar uma página básica de lista dos textos
- Implementar uma página de detalhe de um texto, que gera o HTML anotado do
  texto com erros

## Como instalar o projeto e desenvolver sua solução
Você deve encontrar o esqueleto do problema e sua descrição nesse repositório.

O esqueleto é baseado em `Flask` e `Python 3`, usando `Pipenv` para manejar as
dependências.

Você pode ou não quebrar a solução em vários arquivos; fica a seu critério.

Para instalar o projeto no seu computador você vai precisar:

- Clonar o projeto na sua máquina `git clone ...`
- Instalar o Python 3
- Instalar o `Pipenv`
- Rodar `pipenv shell && pipenv install` na raiz do projeto

Para rodar o servidor de desenvolvimento, rode:
```
FLASK_APP=flask_main.py flask run
```

## O que deve funcionar?

- Marcadores no meio de frases
- Marcadores no começo de frases
- Marcadores no final de frases
- Marcadores em parágrafos arbitrários

## Pontos extras e dicas para uma boa solução

* Minimize o número de queries necessárias
* Adicione testes unitários
* Minimize/explicite o uso de estado local/global
