import sqlite3

conn = sqlite3.connect('./letrus.db')

conn.execute("""
CREATE TABLE texts
  ( id INTEGER PRIMARY KEY
  , body TEXT
  , title VARCHAR(255)
  )
""")

conn.execute("""
CREATE TABLE mistakes
  ( id INTEGER PRIMARY KEY
  , text_id INTEGER
  , start INTEGER
  , end INTEGER
  , paragraph INTEGER
  ,   FOREIGN KEY (text_id) REFERENCES texts(id)
  )
""")
