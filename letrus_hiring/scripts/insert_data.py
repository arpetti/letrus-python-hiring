import sqlite3

conn = sqlite3.connect('./letrus.db')

# Insert texts
conn.execute("""
INSERT INTO texts VALUES
  ( 1
  , 'Meu texto está erado texto aqui asd mais texto aqui\nSegundo palagrafo\nTerceiro paragrafo\nQuarto palagrafo'
  , 'Minha redação 1'
 )
""")

# Insert mistakes
conn.execute("""
INSERT INTO mistakes VALUES
  ( 1
  , 1
  , 15
  , 20
  , 0
  )
""")
conn.execute("""
INSERT INTO mistakes VALUES
  ( 2
  , 1
  , 32
  , 35
  , 0
  )
""")
conn.execute("""
INSERT INTO mistakes VALUES
  ( 3
  , 1
  , 8
  , 16
  , 1
  )
""")
conn.execute("""
INSERT INTO mistakes VALUES
  ( 4
  , 1
  , 7
  , 15
  , 3
  )
""")

conn.commit()
