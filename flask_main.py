from flask import Flask
import sqlite3
from letrus_hiring.marker_html import make_text_html


conn = sqlite3.connect('./letrus.db')
app = Flask(__name__)

@app.route("/")
def list():
    # Puxar dados da tabela `texts`:

    # conn.execute(...)
    # ...

    # Saída esperada:
    return """
    <ul>
      <li>
        <a href="/1">Redação 1</a>
      </li>

      <li>
        <a href="/2">Redação 2</a>
      </li>
    </ul>
    """

@app.route("/<int:text_id>")
def detail(text_id):
    # Puxar dados das tabelas `texts` e `mistakes`:

    # conn.execute(...)
    # ...
    # result = make_text_html(text, mistakes)

    # Saída esperada:
    return """
    <p>
      Minha redação tem erros <em>ortoglaficos</em>.
    </p>

    <p>
      <em>I</em> <em>iscrivi</em> <em>ni</em> língua <em>di</em> para testar.
    </p>

    <p>
      Não temos erros aqui.
    </p>
    """
